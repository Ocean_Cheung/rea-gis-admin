const { override, addWebpackAlias,addDecoratorsLegacy,addLessLoader } = require('customize-cra');
const path = require('path')
const addCustomize = () => config => {
    // 关闭sourceMap
    config.devtool = false;
    // 配置打包后的文件位置
    config.output.publicPath = './';
    
    return config;
  }

module.exports = override(  
    addWebpackAlias({
        '@': path.resolve(__dirname, './src')
    }),
    addDecoratorsLegacy(),
    addCustomize(),
    addLessLoader({
        lessOptions: {
           javascriptEnabled: true,
           localIdentName: '[local]--[hash:base64:5]'
        }
    })
)
