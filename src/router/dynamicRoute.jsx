import Loadable from "react-loadable";
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const Loading = () => <Spin indicator={antIcon} />;
const dynamicRoute = [
  {
    id: "1-1",
    title: "创建任务",
    component: Loadable({
      loader: () => import("@/page/TaskManage/TaskCreate"),
      loading: Loading,
    }),
  },
  {
    id: "1-2",
    title: "待办任务",
    component: Loadable({
      loader: () => import("@/page/TaskManage/TaskDo"),
      loading: Loading,
    }),
  },
  {
    id: "1-3",
    title: "已办任务",
    component: Loadable({
      loader: () => import("@/page/TaskManage/TaskDone"),
      loading: Loading,
    }),
  },
  {
    id: "2-1",
    title: "项目管理",
    component: Loadable({
      loader: () => import("@/page/ProductionManage/ProjectManage"),
      loading: Loading,
    }),
  },
  {
    id: "2-2",
    title: "地图管理",
    component: Loadable({
      loader: () => import("@/page/ProductionManage/Map"),
      loading: Loading,
    }),
  },
  {
    id: "3",
    title: "日志管理",
    component: Loadable({
      loader: () => import("@/page/SystemManage/SystemLog"),
      loading: Loading,
    }),
  },
];

export default dynamicRoute;
