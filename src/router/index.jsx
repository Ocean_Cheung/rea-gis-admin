import React, { Component } from "react";
import { HashRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import Login from "@/page/Login";
import Layout from "@/page/Layout";
import { inject, observer } from "mobx-react";

@inject("permission")
@observer
class AppRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { permission } = this.props;
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={Login}></Route>
          <Route
            path="/"
            render={(props) => {
              if (permission.getToken()) {
                // if (props.location.pathname === "/login") {
                //   return null;
                // }
                return <Layout />;
              } else {
                return <Redirect exact to="/login" />;
              }
            }}
          ></Route>
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;
