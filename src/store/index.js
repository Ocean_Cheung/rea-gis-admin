import test from '@/store/test';
import permission from '@/store/permission';
import layout from '@/store/layout';

const stores = {
  test,
  permission,
  layout
};

export default stores;
