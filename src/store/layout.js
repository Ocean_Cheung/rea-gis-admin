import { observable,action } from "mobx";

class layoutStore{

    @observable collapsed = false;

    @action toggleCollapsed(){
        this.collapsed = !this.collapsed;
    }

}

const layout = new layoutStore();
export default layout