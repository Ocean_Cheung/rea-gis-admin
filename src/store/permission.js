import { observable, flow, action } from "mobx";
import { getMessageApi } from "@/api/menu";
import { generateRoutes,resolveCrumb } from "@/util/parseTree";
import DynamicRoute from "@/router/dynamicRoute";

class permissionStore {
  @observable menuData = [];
  @observable menuRoute = [];
  @observable crumbList = [];
  @observable token = "";

  @action setToken(token){
    this.token = token;
    localStorage.setItem("token",token);
  }

  @action getToken(){
    return localStorage.getItem("token");
  }

  @action setCrumbList(routePath){
    this.crumbList = resolveCrumb(this.menuData,routePath);
  }

  @action clearToken(){
    this.token = "";
    localStorage.setItem("token","");
  }

  fetchMenu = flow(function* () {
    const Mdata = yield getMessageApi();
    const newRoutesData = [];
    generateRoutes(Mdata, DynamicRoute, newRoutesData);
    this.menuData = Mdata;
    this.menuRoute = newRoutesData;
  });
}
const permission = new permissionStore();

export default permission;
