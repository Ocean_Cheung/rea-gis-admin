import { action, computed, observable } from "mobx";

class testStore{

    @observable name = "哈哈哈x";

    @computed get finishedName(){
        return this.name+"111";
    }

    @action changeName(name){
        this.name = name;
    }
}

const test  = new testStore();
export default test;