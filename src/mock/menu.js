const menu = [
  {
    "id": "1",
    "title": "任务管理",
    "name": "TaskManage",
    "children": [
      { "id": "1-1", "title": "创建任务", "name": "TaskCreate", "icon": "","path":"/taskCreate" },
      { "id": "1-2", "title": "待办任务", "name": "TaskDo", "icon": "","path":"/taskDo" },
      { "id": "1-3", "title": "已办任务", "name": "TaskDone", "icon": "","path":"/taskDone" }
    ]
  },
  {
    "id": "2",
    "title": "生产管理",
    "name": "ProductionManage",
    "children": [
      { "id": "2-1", "title": "项目管理", "name": "ProjectManage", "icon": "","path":"/projectManage" }
      /*
       { "id": "2-2", "title": "文件管理", "name": "FileManage", "icon": "","path":"/fileManage" } 
      */
    ]
  },
  {
    "id": "3",
    "title": "日志管理",
    "name": "SystemLog",
    "icon": "",
    "path":"/systemLog"
  }
]

export default menu;
