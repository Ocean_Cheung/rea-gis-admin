import { inject, observer } from "mobx-react";
import React, { Component } from "react";

@inject("test")
@observer
class TaskCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  clickF = () => {
    const { test } = this.props;
    test.changeName("谢谢谢谢");
  };

  async UNSAFE_componentWillMount() {
    console.log(11111111);
  }
  render() {
    const { name } = this.props.test;
    return (
      <div>
        TaskCreate name:{name}
        <button onClick={this.clickF}>测试</button>
      </div>
    );
  }
}

export default TaskCreate;
