import React, { Component } from "react";
import { Form, Input, Button, Checkbox } from 'antd';
import './Login.less'
import { loginApi } from '@/api/menu'
import { inject, observer } from "mobx-react";

@inject("permission")
@observer
class Login extends Component {
  
  constructor(props){
    super(props)
    this.state = {
      username:"",
      password:""
    }
  }

  onFinish = async (values) => {
    const data = await loginApi(values.username,values.password);
    this.props.permission.setToken(data);
    this.props.history.push("/taskManage/taskDo");
  };

  onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  render() {

    const layout = {
      labelCol: {
        span: 4,
      },
      wrapperCol: {
        span: 16,
      },
    };
    const tailLayout = {
      wrapperCol: {
        offset: 4,
        span: 16,
      },
    };

    return (
      <Form
        {...layout}
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={this.onFinish}
        onFinishFailed={this.onFinishFailed}
      >
        <h1 className="loginTitle">登录</h1>
        <Form.Item
          label="用户名"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>
  
        <Form.Item
          label="密码"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
  
        <Form.Item {...tailLayout} name="remember" valuePropName="checked">
          <Checkbox>记住密码</Checkbox>
        </Form.Item>
  
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            登录
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Login;
