import React, { Component } from "react";
import { Menu, Dropdown, Breadcrumb } from "antd";
import { UserOutlined, SettingOutlined, LogoutOutlined, HomeOutlined } from "@ant-design/icons";
import User from "@/assets/images/user.png";
import { withRouter } from "react-router";
import { inject, observer } from "mobx-react";

@inject("permission")
@observer
class TopNav extends Component {
  loginOut = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.props.history.push("/");
  };

  render() {
    const { crumbList } = this.props.permission;
    const menu = (
      <Menu>
        <Menu.Item key="0">
          <span>
            <UserOutlined />
            个人中心
          </span>
        </Menu.Item>
        <Menu.Item key="1">
          <span>
            <SettingOutlined />
            个人设置
          </span>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="2">
          <span onClick={this.loginOut}>
            <LogoutOutlined />
            退出登录
          </span>
        </Menu.Item>
      </Menu>
    );
    return (
      <React.Fragment>
        <div className="leftTop">
          <Breadcrumb>
            <Breadcrumb.Item href="">
              <HomeOutlined />
            </Breadcrumb.Item>
            {crumbList.map((item) => {
              return <Breadcrumb.Item key={item}>{item}</Breadcrumb.Item>;
            })}
          </Breadcrumb>
        </div>
        <div className="rightTop">
          <img src={User} alt="user"></img>
          <Dropdown placement="bottomCenter" overlay={menu}>
            <span onClick={(e) => e.preventDefault()}>张三三</span>
          </Dropdown>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(TopNav);
