import React, { Component } from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import NotFound from "@/page/NotFound";
import SideMenu from "./SideMenu";
import TopNav from "./TopNav";
import "./index.less";

@inject("permission", "layout")
@observer
class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuRoute: [],
      menuData: [],
      selectedKeys: ["1-2"],
      openKeys: ["1"],
    };
  }

  openChange = (openKeys) => {
    this.setState({ openKeys });
  };

  selectMenu = (selectedKeys) => {
    const { history, permission } = this.props;
    this.setState({ selectedKeys });
    permission.setCrumbList(history.location.pathname);
  };

  setKeys = () => {
    const { menuData } = this.props.permission;
    const { selectedKeys, openKeys } = this.getMenuKeys(menuData);
    this.setState({ selectedKeys, openKeys });
  };

  getMenuItem = (menuData, selectedKeys) => {
    menuData.forEach((item) => {
      if (item.children && item.children.length > 0) {
        this.getMenuItem(item.children, selectedKeys);
      } else {
        if (item.path === this.props.location.pathname) {
          selectedKeys.push(item.id);
        }
      }
    });
    return selectedKeys;
  };

  getMenuKeys(menuData) {
    let keys = [];
    const selectedKeys = this.getMenuItem(menuData, keys);
    const openKeys =
      selectedKeys.length > 0 ? [selectedKeys[0].split("-")[0]] : [];
    return { selectedKeys, openKeys };
  }

  async componentDidMount() {
    const { permission, history } = this.props;
    permission.fetchMenu().then(() => {
      const { menuData, menuRoute } = permission;
      const { selectedKeys, openKeys } = this.getMenuKeys(menuData);
      this.setState({ menuData, menuRoute, selectedKeys, openKeys });
      permission.setCrumbList(history.location.pathname);
    });
  }
  render() {
    const { menuData, menuRoute, selectedKeys, openKeys } = this.state;
    const { collapsed } = this.props.layout;
    const leftNavWidth = !collapsed ? "208px" : "64px";
    return (
      <div className="mainDiv">
        <div style={{ width: leftNavWidth }} className="leftNav">
          {
            <SideMenu
              selectMenu={this.selectMenu}
              openChange={this.openChange}
              setKeys={this.setKeys}
              selectedKeys={selectedKeys}
              openKeys={openKeys}
              menuData={menuData}
            ></SideMenu>
          }
        </div>
        <div className="rightNav">
          <div className="topNav">
            <TopNav />
          </div>
          <div className="contentDiv">
            <Switch>
              {menuRoute.map((item) => {
                return (
                  <Route
                    exact
                    key={item.title}
                    path={item.path}
                    component={item.component}
                  />
                );
              })}
              {this.props.location.pathname !== "/" ? (
                <Route component={NotFound} />
              ) : null}
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Layout);

// function MenuItem(props) {
//   const { routes } = props;
//   return (
//     <ul>
//       {routes.map((item) => {
//         return (
//           <React.Fragment key={item.id}>
//             <li>
//               {item.path ? (
//                 <Link to={item.path}>{item.title}</Link>
//               ) : (
//                 <span>{item.title}</span>
//               )}
//             </li>
//             {item.children && item.children.length ? (
//               <MenuItem routes={item.children} />
//             ) : null}
//           </React.Fragment>
//         );
//       })}
//     </ul>
//   );
// }
