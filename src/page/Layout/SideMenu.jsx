import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import IconFont from "@/util/createIconfont";
import Logo from "@/assets/images/logo.png";
import { Menu } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";
const { SubMenu } = Menu;

@inject("layout")
@observer
class SideMenu extends Component {
  toggleCollapsed = () => {
    const { layout,collapsed } = this.props;
    layout.toggleCollapsed();
    !collapsed && this.props.setKeys();
  };

  onOpenChange = (openKeys) => {
    this.props.openChange(openKeys);
  };

  onSelect = ({ selectedKeys }) => {
    this.props.selectMenu(selectedKeys);
  };

  renderMenuItem = (menuData) => {
    return menuData.map((item) => {
      return item.children && item.children.length > 0 ? (
        <SubMenu
          key={item.id}
          title={item.title}
          icon={<IconFont type={item.icon} />}
        >
          {this.renderMenuItem(item.children)}
        </SubMenu>
      ) : (
        <Menu.Item key={item.id} icon={<IconFont type={item.icon} />}>
          <Link to={item.path}>{item.title}</Link>
        </Menu.Item>
      );
    });
  };
  render() {
    const { menuData, layout, selectedKeys, openKeys } = this.props;
    const { collapsed } = layout;
    return (
      <React.Fragment>
        <div className="logoDiv">
          <img src={Logo} alt="logo" />
          {!collapsed ? <h1>Ant Design Pro</h1> : null}
        </div>
        <Menu
          selectedKeys={selectedKeys}
          openKeys={openKeys}
          onOpenChange={this.onOpenChange}
          onSelect={this.onSelect}
          mode="inline"
          theme="dark"
          inlineCollapsed={collapsed}
        >
          {this.renderMenuItem(menuData)}
        </Menu>

        <div
          className="togBtn"
          onClick={this.toggleCollapsed}
          style={{ marginBottom: 16 }}
        >
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(SideMenu);
