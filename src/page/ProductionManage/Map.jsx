import React, { Component } from "react";
import "./Map.less";
import * as esriLoader from "esri-loader";
esriLoader.loadScript({ url: process.env.REACT_APP_ARCGIS_JS });
esriLoader.loadCss(process.env.REACT_APP_ARCGIS_CSS);
class Map extends Component {
  state = {
    map: "",
  };
  initMap = () => {
    const _this = this;
    esriLoader
      .loadModules([
        "esri/map",
        "esri/layers/ArcGISTiledMapServiceLayer",
        "esri/SpatialReference",
        "esri/geometry/Extent",
      ])
      .then(
        async ([Map, ArcGISTiledMapServiceLayer, SpatialReference, Extent]) => {
          const bmap = new Map("map", {
            zoom: 10,
            logo: false,
            slider: false,
          });
          _this.setState(
            {
              map: bmap,
            },
            () => {
              const baseLayer = new ArcGISTiledMapServiceLayer(
                "http://58.216.48.11:6080/arcgis/rest/services/CZ_Vector/MapServer"
              );
              bmap.addLayer(baseLayer);
              bmap.setExtent(
                new Extent(
                  118.82294184077575,
                  31.028681308195175,
                  120.57388791835903,
                  32.21657805886932,
                  new SpatialReference({
                    wkid: 4490,
                  })
                )
              );
            }
          );
        }
      )
      .catch((err) => {
        console.error(err);
      });
  };

  componentDidMount() {
    this.initMap();
  }

  render() {
    return <div className="mapDiv" id="map"></div>;
  }
}

export default Map;
