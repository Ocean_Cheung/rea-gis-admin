import axios from "axios";
import { message } from "antd";
import qs from "qs";
import store from "@/store";
const http = {};
const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_SERVER,
  timeout: 0,
});
// instance.defaults.withCredentials = true;
// 添加请求拦截器
instance.interceptors.request.use(
  function (config) {
    config.headers.Authorization = "Bearer " + store.permission.getToken();
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
// 响应拦截器即异常处理
instance.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (err) => {
    if (err && err.response) {
      switch (err.response.status) {
        case 400:
          err.message = "请求出错";
          break;
        case 401:
          err.message = "授权失败，请重新登录";
          break;
        case 403:
          err.message = "拒绝访问";
          break;
        case 404:
          err.message = "请求错误,未找到该资源";
          break;
        case 500:
          err.message = "服务器端出错";
          break;
        default:
          break;
      }
    } else {
      err.message = "连接服务器失败";
    }
    message.error(err.message);
    setTimeout(() => {
      if (err.response.status === 401) {
        store.permission.clearToken();
        window.location.reload();
      }
    }, 1000);
    return Promise.reject(err.response);
  }
);

function resolveOptions(data, options) {
  if (
    !options ||
    (options &&
      options.headers &&
      options.headers["Content-Type"] === "application/x-www-form-urlencoded")
  ) {
    return qs.stringify(data);
  }
  return data;
}

function getHttp(httpType = "get") {
  return function (url, data, options) {
    const _data = resolveOptions(data, options);
    return new Promise((resolve, reject) => {
      instance[httpType](url, _data, options)
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data);
          } else {
            message.error(response.msg, 10);
            reject(response.msg);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    });
  };
}

http.get = getHttp("get");
http.post = getHttp("post");

export default http;
