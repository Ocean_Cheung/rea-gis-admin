import http from "@/config/httpConfig";

export const loginApi = (username, password) => {
  return http.post("/api/login", { username, password });
};

export const getMessageApi = () => {
  return http.get("/api/getMessage");
};

