//解析路由
export const generateRoutes = (menuData, dyRouteData, newRoutesData) => {
  menuData.forEach(item => {
    dyRouteData.forEach(ele=> {
      if (!item.children || item.children.length === 0) {
        if (item.title === ele.title) {
          item.component = ele.component;
          if (
            newRoutesData.filter((v) => v.title === item.title).length === 0
          ) {
            newRoutesData.push(item);
          }
        }
      }
      if (item.children && item.children.length > 0) {
        generateRoutes(item.children, dyRouteData, newRoutesData);
      }
    });
  });
};
//解析路由面包屑
export const resolveCrumb = (menuData, routePath) => {
  const findCrumbName = (menuData, crumbList, crumbNameList) => {
    menuData.forEach(item => {
      crumbList.forEach(ele=> {
        if (item.path === ele) {
          if(!crumbNameList.includes(item.title)){
            crumbNameList.push(item.title);
          }
        }
        if (item.children && item.children.length > 0) {
          findCrumbName(item.children, crumbList, crumbNameList);
        }
      });
    });
  };
  const findIndex = (routePath) => {
    let index = routePath.indexOf("/");
    let indexArr = [];
    while (index !== -1) {
      indexArr.push(index);
      index = routePath.indexOf("/", index + 1); //利用index为索引，从index+1的位置开始找
    }
    return indexArr;
  };
  if (routePath !== "/") {
    const crumbList = [];
    const crumbNameList = [];
    const indexArr = findIndex(routePath);
    for (let i = 1; i < indexArr.length; i++) {
      const item = routePath.substr(0, indexArr[i]);
      crumbList.push(item);
    }
    crumbList.push(routePath);
    findCrumbName(menuData, crumbList, crumbNameList);
    return crumbNameList;
  }
  return [];
};
