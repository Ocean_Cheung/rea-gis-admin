/** 防抖 以最后一次执行为主 */
export const debounce = (fn, delay = 500) => {
  let timer = null;
  return function () {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fn.apply(this, arguments);
      timer = null;
    }, delay);
  };
};
/**节流 间隔一段时间执行 */
export const throttle = (fn, delay = 500) => {
  let timer = null;
  return function () {
    if (!timer) {
      timer = setTimeout(() => {
        fn.apply(this, arguments);
        timer = null;
      }, delay);
    }
  };
};
